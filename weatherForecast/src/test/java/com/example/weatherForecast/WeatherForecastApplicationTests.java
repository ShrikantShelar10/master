package com.example.weatherForecast;

import com.example.weatherForecast.service.WeatherForecastService;
import com.example.weatherForecast.web.rest.dto.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Assertions;

@SpringBootTest
class WeatherForecastApplicationTests {

	@Mock
	RestTemplate restTemplate;

	@Autowired
	WeatherForecastService weatherForecastService;

	@Test
	public void getArrivals() throws Exception {
		OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();

		WeatherData weatherData = new WeatherData();
		Main main = new Main();
		main.setTemp_min(142.3f);
		weatherData.setDt_txt("2021-02-10 18:00:00");
		weatherData.setMain(main);
		List<WeatherData>  data = new ArrayList<>();
		data.add(weatherData);
		openWeatherResponse.setCod("200");
		openWeatherResponse.setList(data);
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString("https://api.openweathermap.org/data/2.5/forecast")
				.queryParam("zip", "10001")
				.queryParam("appid", "ba9e2057167f315579a5b9f96e822219");

		WeatherForecastService weatherForecastServiceSpy = spy(weatherForecastService) ;
		when(restTemplate.getForObject(builder.toUriString() , OpenWeatherResponse.class)).thenReturn(openWeatherResponse);
		WeatherForecastEntity weatherForecastEntity = weatherForecastServiceSpy.weatherReport("10001");
		Assertions.assertEquals(HttpStatus.OK, weatherForecastEntity.getStatus());
		Assertions.assertNotNull(weatherForecastEntity.getResponseBody().getMinimumTemperatureForDay().getMinimumTemperature());
		Assertions.assertNotNull(weatherForecastEntity.getResponseBody().getCity());

	}

}
