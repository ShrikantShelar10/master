package com.example.weatherForecast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@SpringBootApplication
public class WeatherForecastApplication {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder){

		return restTemplateBuilder
				.setConnectTimeout(Duration.ofSeconds(20))
				.setReadTimeout(Duration.ofSeconds(20))
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(WeatherForecastApplication.class, args);
	}

}
