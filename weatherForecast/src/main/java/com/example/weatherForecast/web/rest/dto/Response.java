package com.example.weatherForecast.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private Object city;
    private MinimumTemperatureForDay minimumTemperatureForDay;
    private List<WeatherReport> weatherReport;
}
