package com.example.weatherForecast.web.rest;

import com.example.weatherForecast.service.WeatherForecast;
import com.example.weatherForecast.web.rest.dto.WeatherForecastEntity;
import com.example.weatherForecast.web.rest.util.EndPointReferrer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(EndPointReferrer.API + EndPointReferrer.WEATHER_FORECAST)
@Validated
public class WeatherForecastResource {

    @Autowired
    WeatherForecast weatherReport;

    Logger logger = LoggerFactory.getLogger(WeatherForecastResource.class);

   @GetMapping(params = {"zip"})
    public ResponseEntity<WeatherForecastEntity> getWeatherData(
            @RequestParam String zip) throws Exception {
        logger.info("getWeatherData is invoked {}", zip);
        return ResponseEntity.status(HttpStatus.OK).body(weatherReport.weatherReport(zip));
    }
}
