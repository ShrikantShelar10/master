package com.example.weatherForecast.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenWeatherResponse {

    private String cod;
    private  String message;
    private  Integer cnt;
    private List<WeatherData> list;
    private Object city ;

}
