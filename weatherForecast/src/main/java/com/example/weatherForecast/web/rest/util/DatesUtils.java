package com.example.weatherForecast.web.rest.util;

import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

public class DatesUtils {

    public static String DateForTomorrow() {
            SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);
            date = calendar.getTime();
            Date finalDate = date;
            return originalFormat.format(finalDate);
    }



}
