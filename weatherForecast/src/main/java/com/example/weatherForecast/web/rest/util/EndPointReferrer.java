package com.example.weatherForecast.web.rest.util;

public class EndPointReferrer {
    public static final String API = "api";
    public static final String WEATHER_FORECAST = "/weatherForecast";
}
