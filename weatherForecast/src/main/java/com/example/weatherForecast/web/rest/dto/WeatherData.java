package com.example.weatherForecast.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WeatherData {

    private Main main;
    private List<Weather> weather;
    private String dt_txt;

}

