package com.example.weatherForecast.service;

import com.example.weatherForecast.web.rest.dto.*;
import com.example.weatherForecast.web.rest.util.DatesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.*;

@Service(value = "dealerService")
public class WeatherForecastService implements  WeatherForecast{

    Logger logger = LoggerFactory.getLogger(WeatherForecastService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${api_key}")
    private String api_key;

    @Value("${url}")
    private String url;

    public WeatherForecastEntity weatherReport(String zip) throws Exception {
        logger.info("WeatherForecastEntity is invoked {}", zip);
        Response response = null;
        WeatherForecastEntity weatherForecastEntity = null;
        try {
            List<WeatherReport> dateAndWeatherArrayList = new ArrayList<>();
            UriComponentsBuilder builder = UriComponentsBuilder
                    .fromUriString(url)
                    .queryParam("zip", zip)
                    .queryParam("appid", api_key);

            OpenWeatherResponse openWeatherResponse = restTemplate.getForObject(builder.toUriString(), OpenWeatherResponse.class);
            openWeatherResponse.getList()
                    .stream()
                    .filter(weatherData -> StringUtils.contains(weatherData.getDt_txt(), DatesUtils.DateForTomorrow()))
                    .forEach(reportForTomorrow ->{
                        WeatherReport dateAndWeather  = new WeatherReport();
                        dateAndWeather.setDate(reportForTomorrow.getDt_txt());
                        dateAndWeather.setMain(reportForTomorrow.getMain());
                        dateAndWeatherArrayList.add(dateAndWeather);
                    });
            WeatherReport weatherReport = dateAndWeatherArrayList.stream().min(Comparator.comparing(report -> report.getMain().getTemp_min())).orElseThrow(NoSuchElementException::new);
            response = Response.builder().minimumTemperatureForDay(MinimumTemperatureForDay.builder().date(weatherReport.getDate()).minimumTemperature(weatherReport.getMain().getTemp_min()).build()).weatherReport(dateAndWeatherArrayList).city(openWeatherResponse.getCity()).build();
            weatherForecastEntity =  WeatherForecastEntity.builder().responseBody(response).status(HttpStatus.OK).build();

        }catch (Exception e){
            weatherForecastEntity =  WeatherForecastEntity.builder().errorMessage(e.getMessage()).build();
        }
        return weatherForecastEntity;
    }


}
