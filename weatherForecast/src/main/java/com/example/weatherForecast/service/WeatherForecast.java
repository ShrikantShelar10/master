package com.example.weatherForecast.service;

import com.example.weatherForecast.web.rest.dto.WeatherForecastEntity;

public interface WeatherForecast {

    public WeatherForecastEntity weatherReport(String zip) throws Exception;
}
