# Weather forecast app

## Getting started

To use this API, you need to register an [API Key](https://openweathermap.org/appid) on the [OpenWeatherApp](https://openweathermap.org/) service.

You can create an ``application-secrets.properties`` in ```src/main/resources``` and add your API key there:

API is to view tomorrow’s predicted temperatures for a given zip-code. Along with tomorrow’s predicted temperatures, API will also identify the coolest hour of the day.

## Command Line Interface (CLI)

### Command Line Input
The data for the execution has been extracted by using curl command with the following options.

```
curl --location --request GET 'http://localhost:8080/api/weatherForecast?zip=15201'
```